# Back-End Coding Assignment for Blue Harvest

This is the back-end coding assignment for Blue Harvest, by Mike Harris.


## Build

Maven is used to build the server.

It can either be built from the command-line (tested on macOS and AWS Linux),
or as a Jenkins pipeline project (running in a Docker container).

The examples below show how to build and run from the command-line.

```
$ mvn package
[INFO] ------------------------------------------------------------------------
[INFO] Building Back-End Coding Assignment for Blue Harvest 0.0.1-SNAPSHOT
[INFO] ------------------------------------------------------------------------
 ...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
```

## Start Server

Spring Boot application is self-contained, no need for a separate web server.
The server will listen on port 8080 by default.

```
$ java -jar target/backend-assignment-0.0.1-SNAPSHOT.jar
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.5.RELEASE)
 ...
2019-06-03 06:15:55.822  INFO 33717 --- [nio-8080-exec-1] o.s.web.servlet.DispatcherServlet        : Completed initialization in 6 ms
```

## Create an account

From a separate command line, use _curl_ to send a POST request to create an
account, with an initial credit of 2000. Note that the customer ID `1234-5678`
is hard-coded (to me) for this demo. Any other customer ID will result in an
exception.

```
$ curl --data-binary '{"customerId":"1234-5678","initialCredit":2000}' -H 'Content-Type: application/json' 'http://localhost:8080/api/v1/account'

{"accountId":1001,"customerId":"1234-5678"}
```

The returned JSON object has the new account number: **1001**.

## Get the detailed account info

Submit a GET request with the account number to get detailed information
about the account: (account number, customer name, and list of transactions).

```
$ curl 'http://localhost:8080/api/v1/account/1001'

{"accountId":1001,"customer":{"id":"1234-5678","name":"Mike","surname":"Harris"},"transactions":[{"accountId":1001,"amount":2000}]}
```

## Add another transaction

Add another transaction for this account, with an amount of 500.

```
$ curl --data-binary '{"accountId":1001,"amount":500}' -H 'Content-Type: application/json' 'http://localhost:8080/api/v1/transaction'
```

No data is returned, but HTTP status `201 Created` is returned.

## Get account info again

Get the account info again, to see the new transaction:

```
$ curl 'http://localhost:8080/api/v1/account/1001'

{"accountId":1001,"customer":{"id":"1234-5678","name":"Mike","surname":"Harris"},"transactions":[{"accountId":1001,"amount":2000},{"accountId":1001,"amount":500}]}
```

## TODO / Future Work

Future work would include:

* API documentation (i.e., Swagger).

* UI to send/display messages using the API.

* More thorough automated code analysis. CheckStyle is being used now, but other static
analysis tools like SpotBugs and JaCoCo for code coverage could be added.
