package com.srsw.blueharvest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot startup class.
 * @author mharris
 */
@SpringBootApplication
public class Application {
    /**
     * Main method to start the Spring Boot application.
     * @param args command-line args
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
