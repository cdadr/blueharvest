package com.srsw.blueharvest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.srsw.blueharvest.db.FakeDatabase;
import com.srsw.blueharvest.model.Account;
import com.srsw.blueharvest.model.Transaction;

/**
 * REST controller class for transactions.
 * @author mharris
 */
@RestController
public class TransactionController {
    /** SLF4J logger. */
    private final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    /** The fake database. */
    private final FakeDatabase database = FakeDatabase.getInstance();

    /**
     * Add a new transaction to an account.
     * POST method, takes as input the transaction (account ID and amount).
     *
     * @param transaction the transaction to add to the account
     * @throws Exception if the account ID is invalid
     */
    @PostMapping(path = "/api/v1/transaction", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void newAccount(@RequestBody final Transaction transaction) throws Exception {
        logger.info("new transaction {}", transaction);
        final Account account = database.findAccount(transaction.getAccountId());
        database.addTransaction(account, transaction.getAmount());
    }
}
