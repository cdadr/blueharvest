package com.srsw.blueharvest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.srsw.blueharvest.db.FakeDatabase;
import com.srsw.blueharvest.model.Account;
import com.srsw.blueharvest.model.AccountDetails;
import com.srsw.blueharvest.model.NewAccountInfo;

/**
 * REST controller class for accounts.
 * @author mharris
 */
@RestController
public class AccountController {
    /** SLF4J logger. */
    private final Logger logger = LoggerFactory.getLogger(AccountController.class);

    /** The fake database. */
    private final FakeDatabase database = FakeDatabase.getInstance();

    /**
     * Create a new account for an existing customer.
     * POST method, takes as input a customer ID and an (optional) initial credit.
     *
     * @param newAccountInfo HTTP Request body containing customer ID and credit
     * @return the newly-created account
     * @throws Exception if an error occurs, e.g., the customer ID is invalid
     */
    @PostMapping(path = "/api/v1/account", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Account newAccount(@RequestBody final NewAccountInfo newAccountInfo) throws Exception {
        logger.info("new account, info={}", newAccountInfo);
        newAccountInfo.validate();

        final Account account = database.createAccount(newAccountInfo.getCustomerId(),
                newAccountInfo.getInitialCredit());
        return account;
    }


    /**
     * Get detailed information about an existing account.
     *
     * @param accountId the account ID (path variable)
     * @return the account
     * @throws Exception if the account ID is invalid
     */
    @GetMapping("/api/v1/account/{accountId}")
    public AccountDetails queryAccount(@PathVariable final int accountId) throws Exception {
        final AccountDetails details = new AccountDetails();
        final Account account = database.findAccount(accountId);
        details.setAccountId(accountId);
        details.setCustomer(account.getCustomer());
        details.setTransactions(database.findTransactions(accountId));
        return details;
    }
}
