package com.srsw.blueharvest.model;

/**
 * The {@code Customer} class represents a banking customer.
 * It has an immutable ID, assigned at creation, and various
 * other fields (name, etc.)
 *
 * @author mharris
 */
public class Customer {
    /** The (immutable) customer ID. */
    private String id;

    /** The customer's given name. */
    private String name;

    /** The customer's surname. */
    private String surname;

    /** Default constructor. */
    public Customer() {
    }

    /**
     * Create a new Customer.
     * @param id the customer ID (immutable)
     * @param name the customer's given name
     * @param surname the customer's surname
     */
    public Customer(final String id, final String name, final String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    /**
     * Get the customer's given name.
     * @return the customer's given name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the customer's given name.
     * @param name the customer's new given name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Get the customer's surname.
     * @return the customer's surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Set the customer's surname.
     * @param surname the customer's new surname
     */
    public void setSurname(final String surname) {
        this.surname = surname;
    }

    /**
     * Get the customer's unique ID.
     * @return the customer ID
     */
    public String getId() {
        return id;
    }

    /**
     * Set the customer's unique ID. This should only be called during
     * deserialization, as the ID should be immutable.
     * @param id the customer's ID.
     */
    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "<customer:id=" + id + ">";
    }
}
