package com.srsw.blueharvest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The {@code Account} class represents a bank account.
 * It is owned by a {@link Customer}.
 *
 * @author mharris
 */
public class Account {
    /** Used to assign account IDs. */
    private static int accountIdGenerator = 1000;

    /** Unique ID for this account. */
    private int accountId;

    /** Customer ID. */
    private String customerId;

    /** The {@code Customer object}. This object is not serialized/deserialized. */
    private transient Customer customer;


    /**
     * Default constructor. Should only be used during deserialization, since the customer ID is
     * required.
     */
    public Account() {
    }

    /**
     *  Create an account for the specified customer ID.
     * @param customerId the customer ID.
     */
    public Account(final String customerId) {
        this.customerId = customerId;
        this.accountId = nextAccountId();
    }

    /**
     * Get the next account ID in sequence.
     * @return a newly-allocated account ID
     */
    private static synchronized int nextAccountId() {
        return ++accountIdGenerator;
    }

    /**
     * Get the account ID.
     * @return the account ID
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Get the customer ID.
     * @return the customer ID
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Set the {@code Customer} object. Not serialized/deserialized.
     * @param customer the Customer object
     */
    @JsonIgnore
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    /**
     * Get the {@code Customer} object. Not serialized/deserialized.
     * @return the Customer object
     */
    @JsonIgnore
    public Customer getCustomer() {
        return customer;
    }
}
