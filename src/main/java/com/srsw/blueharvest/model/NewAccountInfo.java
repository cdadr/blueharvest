package com.srsw.blueharvest.model;

/**
 * Information needed to create a new account. Contains a customer ID and the initial credit.
 *
 * @author mharris
 */
public class NewAccountInfo {
    /** The customer ID. */
    private String customerId;

    /** The initial credit for the account, or zero. */
    private Integer initialCredit;

    /**
     * Set the customer ID.
     * @param customerId the customer ID
     */
    public void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }

    /**
     * Get the customer ID.
     * @return the customer ID
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Get the initial credit.
     * @return the initial credit.
     */
    public Integer getInitialCredit() {
        return initialCredit;
    }

    /**
     * Set the initial credit.
     * @param initialCredit the initial credit.
     */
    public void setInitialCredit(final Integer initialCredit) {
        this.initialCredit = initialCredit;
    }

    /**
     * Validate this object. Checks to ensure that both the customer ID and the initial
     * credit are provided. (The initial credit may be zero, but it must be present).
     * @throws Exception if the validation failed
     */
    public void validate() throws Exception {
        if (customerId == null) {
            throw new Exception("customerId is missing");
        }
        if (initialCredit == null) {
            throw new Exception("initialCredit is missing");
        }
    }

    @Override
    public String toString() {
        return "<customerId=" + customerId + "; initialCredit=" + initialCredit + ">";
    }
}
