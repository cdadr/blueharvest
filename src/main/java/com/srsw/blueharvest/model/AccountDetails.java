package com.srsw.blueharvest.model;

import java.util.List;

/**
 * Detailed information about an account.
 * @author mharris
 */
public class AccountDetails {
    /** The account ID. */
    private int accountId;

    /** The customer who owns this account. */
    private Customer customer;

    /** List of transactions (possibly empty). */
    private List<Transaction> transactions;

    /**
     * Set the account ID.
     * @param accountId the account ID
     */
    public void setAccountId(final int accountId) {
        this.accountId = accountId;
    }

    /**
     * Set the customer who owns this account.
     * @param customer the {@code Customer} object who owns this account
     */
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }

    /**
     * Set the list of transactions this account has.
     * @param transactions the list of transactions
     */
    public void setTransactions(final List<Transaction> transactions) {
        this.transactions = transactions;
    }

    /**
     * Get the account ID.
     * @return the account ID
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Get the customer who owns this account.
     * @return the {@code Customer} object who owns this account
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Get the list of transactions this account has.
     * @return the list of transactions
     */
    public List<Transaction> getTransactions() {
        return transactions;
    }
}
