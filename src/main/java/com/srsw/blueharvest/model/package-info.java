/**
 * Provides model classes for the Blue Harvest back-end coding assignment.
 * <p>
 * The relationships among classes in this package are:<ul>
 *   <li>A {@link Customer} may have zero or more {@link Account}s</li>
 *   <li>An {@link Account} is owned by exactly one {@link Customer}</li>
 *   <li>An {@link Account} may have zero or more {@link Transaction}s</li>
 *   <li>A {@link Transaction} is associated with exactly one {@link Account}</li>
 * </ul>
 */
package com.srsw.blueharvest.model;
