package com.srsw.blueharvest.model;

/**
 * The {@code Transaction} class represents a single transaction within an {@code Account}.
 *
 * @author mharris
 */
public class Transaction {
    /** The account ID this transaction belongs to. */
    private int accountId;
    /** The amount of this transaction. */
    private int amount;

    /** Default constructor. */
    public Transaction() {
    }

    /**
     * Create a new transaction with for the specified account, with the specified amount.
     * @param accountId the account ID
     * @param amount the amount of the transaction
     */
    public Transaction(final int accountId, final int amount) {
        this.accountId = accountId;
        this.amount = amount;
    }

    /**
     * Get the account ID.
     * @return the account ID
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Get the amount of this transaction.
     * @return the amount of this transaction
     */
    public int getAmount() {
        return amount;
    }
}
