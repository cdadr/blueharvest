package com.srsw.blueharvest.db;

/**
 * Thrown when an exception occurs in the fake database.
 * @author mharris
 */
public class DatabaseException extends Exception {
    private static final long serialVersionUID = 8153582495151076754L;

    /**
     * Constructs a new {@code DatabaseException} with no message.
     */
    public DatabaseException() {
    }

    /**
     * Constructs a new {@code DatabaseException} with the specified message.
     * @param message the detail message
     */
    public DatabaseException(final String message) {
        super(message);
    }

    /**
     * Constructs a new {@code DatabaseException} with the specified cause and
     * the message inherited from {@code cause}.
     * @param cause the cause of this exception
     */
    public DatabaseException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new {@code DatabaseException} with the specified message and cause.
     * @param message the detail message
     * @param cause the cause of this exception
     */
    public DatabaseException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
