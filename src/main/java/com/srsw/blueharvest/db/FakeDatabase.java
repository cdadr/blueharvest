package com.srsw.blueharvest.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.srsw.blueharvest.model.Account;
import com.srsw.blueharvest.model.Customer;
import com.srsw.blueharvest.model.Transaction;

/**
 * The "fake database" class. This is used to persist data between API calls, but it is in-memory
 * only, so no data will be saved when the application exits.
 * <p>
 * The initial contents of the database are hard-coded.
 *
 * @author mharris
 */
public final class FakeDatabase {
    /** SLF4J logger. */
    private final Logger logger = LoggerFactory.getLogger(FakeDatabase.class);

    /** The singleton instance of this class. */
    private static FakeDatabase instance = new FakeDatabase();

    /** Customers, by customer ID. */
    private final HashMap<String, Customer> customers = new HashMap<>();

    /** Accounts, by customer ID. */
    private final HashMap<Integer, Account> accounts = new HashMap<>();

    /** Transactions, by account ID. An account has zero or more transactions. */
    private final HashMap<Integer, List<Transaction>> transactions = new HashMap<>();

    /**
     * Get the singleton instance of this class.
     * @return the singleton instance of this class
     */
    public static FakeDatabase getInstance() {
        return instance;
    }

    /**
     * Create and populate the fake database.
     * Marked private to prevent instantiation from other classes.
     */
    private FakeDatabase() {
        customers.put("1234-5678", new Customer("1234-5678", "Mike", "Harris"));
    }

    /**
     * Create a new {@code Account} for a {@code Customer}. If a non-zero initial credit is
     * provided, a new transaction will be added to the account, in the amount specified.
     * @param customerId the ID for an existing customer
     * @param initialCredit an (optional) initial credit.
     * @return a newly-created account
     * @throws DatabaseException if the customer cannot be found
     */
    public Account createAccount(final String customerId, final int initialCredit)
            throws DatabaseException {
        final Customer customer = findCustomer(customerId);
        final Account account = new Account(customerId);
        account.setCustomer(customer);

        // add this account to the accounts map.
        accounts.put(account.getAccountId(), account);

        if (initialCredit != 0) {
            addTransaction(account, initialCredit);
        }

        return account;
    }


    /**
     * Find an account in the "database".
     * @param accountId the account ID
     * @return the account, if found
     * @throws DatabaseException if the account could not be found
     */
    public Account findAccount(final int accountId) throws DatabaseException {
        if (accounts.containsKey(accountId)) {
            return accounts.get(accountId);
        } else {
            throw new DatabaseException("Account ID <" + accountId + "> was not found");
        }
    }


    /**
     * Find a customer in the "database".
     * @param customerId the customer's ID
     * @return the customer, if found
     * @throws DatabaseException if the customer could not be found
     */
    public Customer findCustomer(final String customerId) throws DatabaseException {
        if (customers.containsKey(customerId)) {
            return customers.get(customerId);
        } else {
            throw new DatabaseException("Customer ID <" + customerId + "> was not found");
        }
    }


    /**
     * Add a transaction to the account.
     * @param account the account to add the transaction to
     * @param amount the amount of the transaction
     * @return the newly-created {@code Transaction}
     */
    public Transaction addTransaction(final Account account, final int amount) {
        final Transaction transaction = new Transaction(account.getAccountId(), amount);

        // add to the list of transactions for this account
        final int accountId = account.getAccountId();
        final List<Transaction> list;
        if (transactions.containsKey(accountId)) {
            list = transactions.get(accountId);
        } else {
            list = new ArrayList<>();
            transactions.put(accountId, list);
        }
        list.add(transaction);

        logger.debug("transactions for account {}: {}", accountId, list);
        return transaction;
    }


    /**
     * Find all the transactions for an account. If there are no transactions, an empty list
     * will be returned.
     *
     * @param accountId the account ID
     * @return list of transactions
     * @throws DatabaseException if the account does not exist
     */
    public List<Transaction> findTransactions(final int accountId) throws DatabaseException {
        // first, ensure the account exists (an exception is thrown if it does not)
        @SuppressWarnings("unused")
        final Account account = findAccount(accountId);

        if (transactions.containsKey(accountId)) {
            List<Transaction> list = transactions.get(accountId);
            logger.debug("transactions for account {}: {}", accountId, list);
            return list;
        } else {
            logger.debug("no transactions for account {}", accountId);
            return Collections.emptyList();
        }
    }
}
