/**
 * Classes for the "fake database" to provide persistence between API calls.
 */
package com.srsw.blueharvest.db;
