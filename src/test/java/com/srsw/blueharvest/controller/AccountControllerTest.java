package com.srsw.blueharvest.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.srsw.blueharvest.model.Account;


/** Tests for {@link AccountController}. */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTest {

    /** Spring Test mock MVC. */
    @Autowired
    private MockMvc mvc;

    /** Test all endpoints of AccountController. */
    @Test
    public void testAccount() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        
        // create an account with an initial credit
        final String jsonOut = "{\"customerId\":\"1234-5678\",\"initialCredit\":5555}";
        final MvcResult result = mvc.perform(post("/api/v1/account").contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonOut))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.customerId").value("1234-5678"))
                .andReturn();
        final Account account = mapper.readValue(result.getResponse().getContentAsByteArray(), Account.class);
        final int accountId = account.getAccountId();
        
        // verify the account details
        mvc.perform(get("/api/v1/account/" + accountId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.accountId").value(accountId))
                .andExpect(jsonPath("$.customer.name").value("Mike"))
                .andExpect(jsonPath("$.customer.surname").value("Harris"))
                .andExpect(jsonPath("$.transactions.length()").value(1))
                .andExpect(jsonPath("$.transactions[0].amount").value(5555));
    }
}
