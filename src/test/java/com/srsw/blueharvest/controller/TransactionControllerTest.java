package com.srsw.blueharvest.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.srsw.blueharvest.model.Account;
import com.srsw.blueharvest.model.Transaction;


/** Tests for {@link TransactionController}. */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTest {

    /** Spring Test mock MVC. */
    @Autowired
    private MockMvc mvc;

    /** Test all endpoints of TransactionController. */
    @Test
    public void testTransaction() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        
        // create an account with no credit
        String jsonOut = "{\"customerId\":\"1234-5678\",\"initialCredit\":0}";
        final MvcResult result = mvc.perform(post("/api/v1/account").contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonOut))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        final Account account = mapper.readValue(result.getResponse().getContentAsByteArray(), Account.class);
        final int accountId = account.getAccountId();

        // add a transaction
        Transaction transaction = new Transaction(accountId, 1234);
        String jsonOut2 = mapper.writeValueAsString(transaction);
        mvc.perform(post("/api/v1/transaction").contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonOut2))
            .andExpect(status().isCreated());

        // verify the transaction is present
        mvc.perform(get("/api/v1/account/" + accountId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.transactions.length()").value(1))
                .andExpect(jsonPath("$.transactions[0].amount").value(1234));
    }
}
