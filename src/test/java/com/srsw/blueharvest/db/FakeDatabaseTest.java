package com.srsw.blueharvest.db;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.srsw.blueharvest.model.Account;
import com.srsw.blueharvest.model.Customer;
import com.srsw.blueharvest.model.Transaction;

/** Tests for {@link FakeDatabase}. */
public class FakeDatabaseTest {
    private static final String TEST_CUSTOMER_ID = "1234-5678";

    /** Fake database under test. */
    private FakeDatabase database;

    @Before
    public void setup() {
        database = FakeDatabase.getInstance();
    }

    /** Just makes sure the database is not null. */
    @Test
    public void testGetInstance() {
        assertNotNull("could not create fake database", database);
    }

    @Test
    public void testCreateAccountWithCredit() throws Exception {
        final int TEST_AMOUNT = 10000;
        final Account account = database.createAccount(TEST_CUSTOMER_ID, TEST_AMOUNT);
        assertNotNull("account is null", account);
        assertEquals("customer ID mismatch:", TEST_CUSTOMER_ID, account.getCustomerId());
        final int accountId = account.getAccountId();
        
        final List<Transaction> transactions = database.findTransactions(accountId);
        assertNotNull(transactions);
        assertEquals("wrong number of transactions:", 1, transactions.size());
        final Transaction transaction = transactions.get(0);
        assertNotNull(transaction);
        assertEquals("account ID mismatch in transaction:", accountId, transaction.getAccountId());
        assertEquals("amount mismatch in transaction:", TEST_AMOUNT, transaction.getAmount());
    }


    @Test
    public void testCreateAccountWithoutCredit() throws Exception {
        final Account account = database.createAccount(TEST_CUSTOMER_ID, 0);
        assertNotNull("account is null", account);
        assertEquals("customer ID mismatch:", TEST_CUSTOMER_ID, account.getCustomerId());
        final int accountId = account.getAccountId();
        
        final List<Transaction> transactions = database.findTransactions(accountId);
        assertNotNull(transactions);
        assertEquals("wrong number of transactions:", 0, transactions.size());
    }

    @Test
    public void testFindAccount() throws Exception {
        final Account account = database.createAccount(TEST_CUSTOMER_ID, 0);
        assertNotNull("account is null", account);
        final int accountId = account.getAccountId();
        
        Account account2 = database.findAccount(accountId);
        assertNotNull("could not find account", account2);
        
        assertEquals("accounts do not match:", account, account2);
    }

    @Test
    public void testFindCustomer() throws Exception {
        Customer customer = database.findCustomer(TEST_CUSTOMER_ID);
        assertNotNull("customer is null", customer);
        assertEquals("Customer ID mismatch:", TEST_CUSTOMER_ID, customer.getId());
    }

    @Test
    public void testAddTransaction() throws Exception {
        final int TEST_AMOUNT = 12345;
        final Account account = database.createAccount(TEST_CUSTOMER_ID, 0);
        assertNotNull("account is null", account);
        final int accountId = account.getAccountId();
        
        final List<Transaction> transactions = database.findTransactions(accountId);
        assertNotNull(transactions);
        assertEquals("wrong number of transactions:", 0, transactions.size());
        
        Transaction transaction = database.addTransaction(account, TEST_AMOUNT);
        assertNotNull(transaction);
        assertEquals("Transaction account id mismatch:", accountId, transaction.getAccountId());
        assertEquals("Transaction amount mismatch:", TEST_AMOUNT, transaction.getAmount());

        final List<Transaction> transactions2 = database.findTransactions(accountId);
        assertNotNull(transactions2);
        assertEquals("wrong number of transactions:", 1, transactions2.size());
        final Transaction transaction2 = transactions2.get(0);
        assertNotNull(transaction2);
        assertEquals("transactions do not match", transaction, transaction2);
    }

    @Test
    public void testFindTransactions() throws Exception {
        final int TEST_AMOUNT1 = 9999;
        final int TEST_AMOUNT2 = 1111;
        final Account account = database.createAccount(TEST_CUSTOMER_ID, TEST_AMOUNT1);
        assertNotNull("account is null", account);
        final int accountId = account.getAccountId();
        
        final List<Transaction> transactions = database.findTransactions(accountId);
        assertNotNull(transactions);
        assertEquals("wrong number of transactions:", 1, transactions.size());

        database.addTransaction(account, TEST_AMOUNT2);
        final List<Transaction> transactions2 = database.findTransactions(accountId);
        assertNotNull(transactions2);
        assertEquals("wrong number of transactions:", 2, transactions2.size());
    }
}
